#!/usr/bin/env python
# coding: utf-8
# %%
import tensorflow as tf

from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import LSTM
from tensorflow.keras.constraints import NonNeg 

from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import mean_squared_error, r2_score
from sklearn.preprocessing import StandardScaler
from sklearn import preprocessing




# %% [markdown]
# $\underline{\text{Kernel Constraints:}}$

# %%


class NonNegativeCumsum(tf.keras.constraints.Constraint):

    def __call__(self, w):
        return w * tf.cast(tf.math.greater_equal(tf.math.cumsum(w), 0.), w.dtype)
    
class NonNegativeCumsum2(tf.keras.constraints.Constraint):

    def __call__(self, w):
        w1 = w[:1001] * tf.cast(tf.math.greater_equal(tf.math.cumsum(w[:1001]), 0.), w.dtype)
        w2 = w[1001:] * tf.cast(tf.math.greater_equal(tf.math.cumsum(w[1001:]), 0.), w.dtype)
        y = tf.concat([w1, w2], 0)
        return tf.reshape(y, [w.shape[0],-1])




# %% [markdown]
# $\underline{\text{Personnal Layers:}}$

# %%
class PowerLayer(tf.keras.layers.Layer):
    '''ma class layer personalisée '''
    def __init__(self, output_dim, **kwargs):
        ''' initialisation, avec les dimensions d'entrée et sortie '''
        self.output_dim = output_dim
        super(PowerLayer, self).__init__(**kwargs)
        
    def build(self, input_shape):
        ''' construction/définition des poids du layer '''        
        self.kernel = self.add_weight(name='kernel', shape = (1,), 
                                      initializer = tf.keras.initializers.Constant(value = 0.5),
                                      constraint = tf.keras.constraints.MaxNorm(max_value = 1, axis = 0),
                                      trainable = True)
        super(PowerLayer, self).build((1,))
        
    def call(self, input_data):
        ''' traitement proprement dit du layer '''
        ''' renvoie le produit 
        '''        
        return tf.sign(input_data)*tf.pow(tf.abs(input_data), self.kernel)
    
    def compute_output_shape(self, input_shape):
        ''' calcul du format de sortie de la couche '''          
        return (input_shape[0],1)


# %%
class DoublePowerLayer(tf.keras.layers.Layer):
    '''ma class layer personalisée '''
    def __init__(self, output_dim, **kwargs):
        ''' initialisation, avec les dimensions d'entrée et sortie '''
        self.output_dim = output_dim
        super(DoublePowerLayer, self).__init__(**kwargs)
        
    def build(self, input_shape):
        ''' construction/définition des poids du layer '''        
        self.kernel = self.add_weight(name = 'kernel', shape = (2,), 
                                      initializer = tf.keras.initializers.RandomUniform(minval = 0., maxval = 1.), 
                                      constraint = tf.keras.constraints.MaxNorm(max_value = 1, axis = 0),
                                      trainable = True)
        super(DoublePowerLayer, self).build((2,))
        
    def call(self, input_data):
        ''' traitement proprement dit du layer '''
        ''' renvoie le vecteur concatene
        ''' 
        max_lag = int((len(input_data)-1)/2)
        
        t1 = tf.math.multiply(tf.sign(input_data[:max_lag+1]),tf.pow(tf.abs(input_data[:max_lag+1]), self.kernel[0]))
        t2 = tf.math.multiply(tf.sign(input_data[max_lag+1:]),tf.pow(tf.abs(input_data[max_lag+1:]), self.kernel[1]))
        
        return tf.concat([t1, t2], 0)
                          
    def compute_output_shape(self, input_shape):
        ''' calcul du format de sortie de la couche '''          
        return (self.output_dim,1)


# %%
class DoublePowerLayer2(tf.keras.layers.Layer):
    '''ma class layer personalisée '''
    def __init__(self, output_dim, **kwargs):
        ''' initialisation, avec les dimensions d'entrée et sortie '''
        self.output_dim = output_dim
        super(DoublePowerLayer2, self).__init__(**kwargs)
        
    def build(self, input_shape):
        ''' construction/définition des poids du layer '''        
        self.kernel = self.add_weight(name = 'kernel', shape = (2,), 
                                      initializer = tf.keras.initializers.RandomUniform(minval = 0., maxval = 1.), 
                                      constraint = tf.keras.constraints.MaxNorm(max_value = 1, axis = 0),
                                      trainable = True)
        super(DoublePowerLayer2, self).build((2,))
        
        
    def call(self, input_data):
        ''' traitement proprement dit du layer '''
        ''' renvoie le vecteur concatene
        ''' 
        t1 = tf.math.multiply(tf.sign(input_data),tf.pow(tf.abs(input_data), self.kernel[0]))
        t2 = tf.math.multiply(tf.sign(input_data),tf.pow(tf.abs(input_data), self.kernel[1]))
        t3 = tf.concat([t1, t2], 0)        
        return t3
                          
    def compute_output_shape(self, input_shape):
        ''' calcul du format de sortie de la couche '''          
        return (self.output_dim,1)


# %% [markdown]
# $\underline{\text{Models:}}$

# %%
def modelPowerLaw(trainX, trainY, optimizer, batch_size, symmetry = False, epochs = 200, validation_split = 0.3, verbose = 1, use_bias = False):
    
    if symmetry == False:
        
        n1 = trainX.shape[1]
    
        model = Sequential()
        model.add(PowerLayer(output_dim = n1)
                  )
                        
        model.add(tf.keras.layers.Dense(units = 1, 
                                        kernel_constraint = NonNegativeCumsum(), 
                                        use_bias = use_bias,
                                        dtype = 'float64'
                                        )
                 )


        model.compile(optimizer = optimizer,
                             loss = "mean_squared_error")
        
        history = model.fit(trainX,
                        trainY,
                        epochs = epochs,
                        # We do not suppress logging.
                        verbose = 1,
                        # Calculate validation results on 30% of the training data.
                        validation_split = 0.3,
                        batch_size = batch_size#,
                        #callbacks = [callback]
                        )
        
    else:
        
        n, d = trainX.shape

        # path1
        input1 = tf.keras.layers.Input(shape = (d,))
        x1 = PowerLayer(output_dim = d)(input1)
        x1 = tf.keras.layers.Dense(units = 1,
                               kernel_constraint = NonNegativeCumsum(),
                               use_bias = False,
                               dtype = 'float64'
                               )(x1)

        # path2
        input2 = tf.keras.layers.Input(shape = (d,))
        x2 = PowerLayer(output_dim = d)(input2)
        x2 = tf.keras.layers.Dense(units = 1,
                               kernel_constraint = NonNegativeCumsum(),
                               use_bias = False,
                               dtype = 'float64'
                               )(x2)
        # average
        #avg = tf.keras.layers.Average(dtype = 'float64')([x1, x2])
        model = tf.keras.models.Model(inputs = [input1, input2], outputs = [x1,x2])

        # compilation
        model.compile(optimizer=optimizer,
                  loss="mean_squared_error")

        history = model.fit([trainX, -trainX],
                        [trainY, -trainY],
                        epochs = epochs,
                        # We do not suppress logging.
                        verbose = 1,
                        # Calculate validation results on 30% of the training data.
                        validation_split = 0.3,
                        batch_size = batch_size#,
                        #callbacks = [callback]
                        )

    model.summary()
    
    return model, history


# %%
def modelDoublePowerLaw(trainX, trainY, optimizer, batch_size,symmetry = False, epochs = 200, validation_split = 0.3, verbose = 1, use_bias = False):
    
    if symmetry == False:
    
        n1 = trainX.shape[1]
    
        model = Sequential()
    
        model.add(DoublePowerLayer(output_dim = n1))
        model.add(tf.keras.layers.Dropout(0.2))
        model.add(tf.keras.layers.Dense(units = 1, 
                                    kernel_constraint = NonNegativeCumsum2(), 
                                    kernel_initializer=tf.keras.initializers.RandomNormal(stddev=0.5),
                                    use_bias = use_bias,
                                    dtype = 'float64')
                        )
    


        model.compile(optimizer = optimizer,
                loss = "mean_squared_error")
        
        history = model.fit(trainX,
                        trainY,
                        epochs = epochs,
                        # We do not suppress logging.
                        verbose = 1,
                        # Calculate validation results on 30% of the training data.
                        validation_split = 0.3,
                        batch_size = batch_size#,
                        #callbacks = [callback]
                        )
        
    else: 
        
        n, d = trainX.shape

        # path1
        input1 = tf.keras.layers.Input(shape = (d,))
        x1 = DoublePowerLayer(output_dim = d)(input1)
        x1 = tf.keras.layers.Dense(units = 1,
                               kernel_constraint = NonNegativeCumsum(),
                               kernel_initializer=tf.keras.initializers.RandomNormal(stddev=0.5),
                               use_bias = False,
                               dtype = 'float64'
                               )(x1)

        # path2
        input2 = tf.keras.layers.Input(shape = (d,))
        x2 = DoublePowerLayer(output_dim = d)(input2)
        x2 = tf.keras.layers.Dense(units = 1,
                               kernel_constraint = NonNegativeCumsum(),
                               kernel_initializer=tf.keras.initializers.RandomNormal(stddev=0.5),
                               use_bias = False,
                               dtype = 'float64'
                               )(x2)
        # average
        #avg = tf.keras.layers.Average(dtype = 'float64')([x1, x2])
        model = tf.keras.models.Model(inputs = [input1, input2], outputs = [x1,x2])

        # compilation
        model.compile(optimizer = optimizer,
                  loss = "mean_squared_error")

        history = model.fit([trainX, -trainX],
                        [trainY,-trainY],
                        epochs = epochs,
                        # We do not suppress logging.
                        verbose = 1,
                        # Calculate validation results on 30% of the training data.
                        validation_split = 0.3,
                        batch_size = batch_size#,
                        #callbacks = [callback]
                        )

    
    model.summary()
    
    return model, history


# %%
