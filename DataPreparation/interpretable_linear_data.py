import numpy as np 
import pandas as pd
import os
from tqdm.auto import tqdm
import tqdm
import typing
from sklearn.preprocessing import StandardScaler

# generate script
def get_script(prgs, start_date, end_date, root=None):
    dates = [date.date().isoformat() for date in pd.date_range(start=start_date, end=end_date, freq='B')]
    script = "#!/usr/bin/env bash\n"
    files = dict()
    for prg in prgs:
        script += f"# Dumping product {prg}\n"
        for date in dates:
            script += f"# Dumping date {date}\n"
            cmd = f"ydump -s prg:{prg}" "{CFM}" f" -b {date} -e {date} -d trades -o {root}/{prg}_{date}.h5 --overwrite 1"
            script += cmd + '\n'
            files[(prg, date)] = os.path.join(root, f'{prg}_{date}.h5')
    with open(os.path.join(root, 'script.sh'), 'w') as f:
        f.write(script)
    return script, files, dates


# $\underline{\text{Data cleaning function}}$

# +
# we use this function to clean the data daily

def data_cleaner(trades, price_max, price_min, dastart, daend, time_bins, aggregation, normalization, order_type):
    
    '''
    trades: a dataframe that contains all the data dumped with get_script
    
    we want to clean the data: 
        - add mid price column, sign column, and a qty signed column 
        - change the time from UTC to chicago/america 
        - get rid of non-reasonable bid-ask spreads, non reasonable prices ( 0 < Price < 200 )
        - Merge trades triggered by the same market order 
        - select the most efficient time interval (liquidity + enough data)
    We could try to normalize the data: for this we propose a normalized mode and a non-normalized mode. 
    ''' 
    
    # we add the columns 
    trades['mid'] = 0.5 * (trades['bid'] + trades['ask'])
    trades['sign'] = np.sign(trades['price'] - trades['mid'])
    trades['qty_signed'] = trades['qty'] * trades['sign']
    
    # we add an eastern time column 
    trades['ts_market_et'] = trades['ts_market'].dt.tz_localize('UTC').dt.tz_convert('America/Chicago')
    
    # we select the wanted hours of trading 
    trades = trades.set_index('ts_market_et').loc[dastart:daend]
    
    # we select reasonable prices 
    trades = trades.loc[(trades['price'] > price_min) & (trades['price'] < price_max)]
    # we select good bid/ask lines, ie ask-bid > 0 
    trades = trades[trades['ask'] > trades['bid']]
        
    #we merge trades triggered by the same order 
    if aggregation: 
        
        trades = trades.groupby(['sign', 'ts_market_et',
                 'price', 'condition', 'side']).agg({'qty': 'sum', 'ask': 'first',
                                               'bid': 'first','side':'first',
                                               'volume':'first', 'mmtCodes': 'first', 
                                               'evt_seq_num': 'first', 'ts_recv': 'first', 
                                               'provider': 'first', 'source': 'first', 
                                               'ts_market': 'first', 'type': 'first', 
                                               'bid_size': 'first', 'ask_size': 'first', 
                                               'mid': 'first', 'qty_signed': 'sum',
                                               'volume': 'last', 'vwap': 'first', 
                                                'trade_id': 'first', 'order_id': 'first'}) 
    # we get back to the daytime order in our time series
    trades= trades.sort_values(by = ['ts_recv', 'evt_seq_num'])
    
    # we normalize the qtys
    if normalization: 
        
        # volume normalization 
        vol_std = trades['qty_signed'].std()
        #vol_mean = trades['qty_signed'].mean()
        vol_sum = trades['qty_signed'].sum()
        trades['qty_signed_normalized'] = (trades['qty_signed']).div(vol_std)
        
        '''# bid normalization 
        bid_std = trades['bid_size'].std()
        bid_mean = trades['bid_size'].mean()
        trades['bid_size_norm'] = (trades['bid_size']-bid_mean).div(bid_std)
        
        # ask normalization 
        ask_std = trades['ask_size'].std()
        ask_mean = trades['ask_size'].mean()
        trades['ask_size_norm'] = (trades['ask_size']-vol_mean).div(ask_std)'''
        
        
    # allows to differenciate agressive trades and non-agressive trades / price changing vs non prie changing, voir papiers 
    if order_type:
        
        conditions = [
    (trades['qty_signed'] < 0) & (trades['qty_signed']+trades['bid_size'] <= 0), # we put an agressive bid market order 
    (trades['qty_signed'] < 0) & (trades['qty_signed']+trades['bid_size'] > 0), # we put an no-agressive bid market order 
    (trades['qty_signed'] > 0) & (trades['qty_signed']-trades['ask_size'] >= 0), # we put an agressive ask market order 
    (trades['qty_signed'] > 0) & (trades['qty_signed']-trades['ask_size'] < 0) # we put an no-agressive ask market order 
    ]
        values = [1,0,1,0] 
        
        trades['order_type'] = np.select(conditions, values)

    
    return trades


# -
def data_cleaner2(trades_dict, price_max, price_min, dates):
    
    '''
    trades: a dataframe that contains all the data dumped with get_script
    
    we want to clean the data: 
        - add mid price column, sign column, and a qty signed column 
        - change the time from UTC to chicago/america 
        - get rid of non-reasonable bid-ask spreads, non reasonable prices ( 0 < Price < 200 )
        - Merge trades triggered by the same market order 
        - select the most efficient time interval (liquidity + enough data)
    We could try to normalize the data: for this we propose a normalized mode and a non-normalized mode. 
    ''' 
    cpt = 0
    for day in dates:
        trades_dict[day]['mid'] = 0.5 * (trades_dict[day]['bid'] + trades_dict[day]['ask'])
        trades_dict[day]['sign'] = np.sign(trades_dict[day]['price'] - trades_dict[day]['mid'])
        trades_dict[day]['qty_signed'] = trades_dict[day]['qty'] * trades_dict[day]['sign']
        trades_dict[day]['ts_market_et'] = trades_dict[day]['ts_market'].dt.tz_localize('UTC').dt.tz_convert('America/Chicago')
        trades_dict[day] = trades_dict[day].loc[(trades_dict[day]['price'] > price_min) 
                                                & (trades_dict[day]['price'] < price_max)]
        cpt += trades_dict[day].shape[0]
        trades_dict[day] = trades_dict[day][trades_dict[day]['ask'] > trades_dict[day]['bid']]
        trades_dict[day] = trades_dict[day].groupby(['sign', 'ts_market_et',
                 'price', 'condition', 'side']).agg({'qty': 'sum', 'ask': 'first',
                                               'bid': 'first',
                                               'volume':'first', 'mmtCodes': 'first', 
                                               'evt_seq_num': 'first', 'ts_recv': 'first', 
                                               'provider': 'first', 'source': 'first', 
                                               'ts_market': 'first', 'type': 'first', 
                                               'bid_size': 'first', 'ask_size': 'first', 
                                               'mid': 'first', 'qty_signed': 'sum',
                                               'volume': 'last', 'vwap': 'first', 
                                                'trade_id': 'first', 'order_id': 'first'}) 
        trades_dict[day] = trades_dict[day].sort_values(by = ['ts_recv', 'evt_seq_num'])
        
        trades_returns = trades_dict[day].mid.diff().shift(-1).copy()
        trades_dict[day].drop(index = trades_dict[day].index[-1], 
                axis = 0, 
                inplace=True)
        trades_dict[day]['returns'] =  trades_returns.dropna()
        

    df = trades_dict[dates[0]].copy()
    for day in dates[1:]:
        df = pd.concat( (df, trades_dict[day]) )
        
    df["hour"] = df["ts_recv"].dt.hour
    df["qty_signed_squared"] = df["qty_signed"]**2
    
    dfnew = df.groupby(['hour']).agg({'qty_signed_squared': 'sum'})
    dfnew['qty_signed_squared'] = dfnew['qty_signed_squared']
    dfnew.reset_index(inplace = True)
    
    df = pd.merge(df, dfnew, right_on = 'hour', left_on = 'hour', how = 'left' )
        
    df['qty_signed'] = df['qty_signed'].div(df['qty_signed_squared_y'])
    
    #df = df.drop(columns=['qty_signed_squared_x', 'qty_signed_squared_y', 'hour'])
    df = df.drop(columns=['qty_signed_squared_x', 'qty_signed_squared_y'])
    
    print(cpt)
    print(df.shape[0])
    
    return df


def data_cleaner3(trades, price_max, price_min):
    
    '''
    trades: a dataframe that contains all the data dumped with get_script
    
    we want to clean the data: 
        - add mid price column, sign column, and a qty signed column 
        - change the time from UTC to chicago/america 
        - get rid of non-reasonable bid-ask spreads, non reasonable prices ( pmin < Price < pmax )
        - Merge trades triggered by the same market order 
        - select the most efficient time interval (liquidity + enough data)
    We could try to normalize the data: for this we propose a normalized mode and a non-normalized mode. 
    ''' 
    
    trades['mid'] = 0.5 * (trades['bid'] + trades['ask'])
    trades['sign'] = np.sign(trades['price'] - trades['mid'])
    trades['qty_signed'] = trades['qty'] * trades['sign']
    trades['ts_market_et'] = trades['ts_market'].dt.tz_localize('UTC').dt.tz_convert('America/Chicago')
    trades = trades.loc[(trades['price'] > price_min) & (trades['price'] < price_max)]
    trades = trades[trades['ask'] > trades['bid']]
    trades = trades.groupby(['sign', 'ts_market_et',
                 'price', 'condition']).agg({'qty': 'sum', 'ask': 'first',
                                               'bid': 'first',
                                               'evt_seq_num': 'first', 'ts_recv': 'first', 
                                               'ts_market': 'first', 'type': 'first', 
                                               'bid_size': 'first', 'ask_size': 'first', 
                                               'mid': 'first', 'qty_signed': 'sum',
                                               'volume': 'last'}) 
    trades = trades.sort_values(by = ['ts_recv', 'evt_seq_num'])
    
    print(trades.shape)
    
    trades_returns = trades.mid.diff().shift(-1).copy()
    print(len(trades_returns))
    trades.drop(index = trades.index[-1], 
                axis = 0, 
                inplace=True)
    trades['returns'] =  trades_returns.dropna()
    #trades =  trades[ trades['returns'].notna()]
    
    print(trades.shape)
        
    trades["hour"] = trades["ts_recv"].dt.hour
    trades["qty_signed_squared"] = trades["qty_signed"]**2
    print(trades.shape)
    
    dfnew = trades.groupby(['hour']).agg({'qty_signed_squared': 'sum'})
    dfnew['qty_signed_squared'] = dfnew['qty_signed_squared']
    dfnew.reset_index(inplace = True)
    
    trades = pd.merge(trades, dfnew, right_on = 'hour', left_on = 'hour', how = 'left' )
    print(trades.shape)
    trades['qty_signed'] = trades['qty_signed'].div(trades['qty_signed_squared_y'])
    print(trades.shape)
    #trades = trades.drop(columns=['qty_signed_squared_x', 'qty_signed_squared_y'], inplace = True)
    print(trades.shape)
    return trades
