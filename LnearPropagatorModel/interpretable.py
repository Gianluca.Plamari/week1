import numpy as np 
import typing
import pandas as pd

from tqdm.auto import tqdm

# $\underline{\text{Stylized facts functions}}$

# autocovariance function
def autocovariance(data: typing.Union[np.array, pd.Series],  max_lag: int = 1000) -> pd.Series:

    ret = pd.Series(dtype=np.float)
    data = pd.Series(data)

    for lag in tqdm(range(max_lag+1)):

        ret.loc[lag] = np.mean((data.shift(lag, fill_value=0)).values*data.values)

    return pd.Series(ret)

# we define some toy kernels
def kernel(N: int , typ: int , gamma: int , beta: int ) -> pd.Series:
    if typ == 0:
        return(pd.Series(np.ones(N)))
    elif typ == 1:
        return(pd.Series(np.arange(N)))
    elif typ == 2:
        return(pd.Series(np.zeros(N)))
    elif typ == 3:
        return(pd.Series(np.exp(-np.arange(N)/20)))
    else:
        return(pd.Series(gamma/(np.power(np.arange(1,N+1), beta))))

# we define the matrix that defines the propagator model equation WITH THE KERNEL
def kernel_matrix(K: typing.Union[np.array, pd.Series], max_lag: int) -> np.array:
    
    K = pd.Series(K)
    matrix = np.zeros((max_lag,max_lag))
    
    for i in range(1,max_lag):
        matrix[i:i+1,:i] = K.iloc[:i].values[::-1]
    return matrix

# we define the matrix that defines the propagator model equation WITH THE ODRERFLOWS
def orderflows_matrix(K: typing.Union[np.array, pd.Series], max_lag: int) -> np.array:
    
    K = pd.Series(K)
    matrix = K[0]*np.eye(max_lag)
    
    for i in range(1,max_lag):

        matrix[i:i+1,0] = K[i]
        matrix[i:i+1,1:i+1] = K.iloc[:i].values[::-1]

    return matrix

# we define the covariance matrix with a covariance vector 
def covariance_matrix(correl_vector: typing.Union[np.array, pd.Series], L: int) -> np.array:

    
    C = correl_vector[0]*np.eye(L+1)

    for i in range(L):

        C[i:i+1,i:] = correl_vector[:L+1-i]

        
    return (C+C.T-correl_vector[0]*np.eye(L+1))


def response_function(data1: typing.Union[np.array, pd.Series], data2: typing.Union[np.array, pd.Series], max_lag: int = 1000) -> pd.Series:

     
    '''data1 represents the order flow
    data2 represents the mid price
    '''
    

    data1 = pd.Series(data1)
    data2 = pd.Series(data2)

    response = np.zeros(max_lag+1)

    for lag in tqdm(range(1,max_lag+1)):

        response[lag] = (data1*(data2.shift(-lag)-data2)).mean()

    return pd.Series(response)

# we give the sign-retrun correlation 
def sign_return_correlation(K: typing.Union[np.array, pd.Series], C: typing.Union[np.array, pd.Series], max_lag: int = 1000) -> pd.Series:

    K = pd.Series(K)
    C = pd.Series(C)
    
    S = np.zeros(max_lag+1)

    matrix = covariance_matrix(C, max_lag)

    for l in range(max_lag+1):

        for i in range(max_lag+1):

            S[l] += K.values[i]*matrix[l,i]
    
    return(pd.Series(S))

def sign_return_correlation2(r: typing.Union[np.array, pd.Series], epsilon: typing.Union[np.array, pd.Series], max_lag: int = 1000) -> pd.Series:

    r = pd.Series(r)
    epsilon = pd.Series(epsilon)
    
    S = pd.Series(dtype = np.float)

    for lag in tqdm(range(max_lag)):

        S.loc[lag] = np.mean((r.shift(-lag, fill_value=0)).values*epsilon.values)

    return pd.Series(S)


# variogram function
def variogram2(data: typing.Union[np.array, pd.Series], max_lag: int = 1000) -> pd.Series:

    '''

    data is in general the midprice observed by the market at each time of discretization
    max_lag represents the maximum lag considered by the user 
    
    '''

    vario = pd.Series(dtype = np.float)
    data = pd.Series(data)

    N = len(data)

    for lag in tqdm(range(max_lag)):
        #vario.loc[lag] = np.var(data.values[lag:N]-data[:N-lag])
        vario.loc[lag] = np.var(data.shift(-lag)-data)
    
    return vario


def generator(orderflow: typing.Union[np.array, pd.Series], kernel: typing.Union[np.array, pd.Series], noise: typing.Union[np.array, pd.Series], max_lag: int = 1000):
    
    '''
    ordeflow is a the total orderflow vector, we need it for the returns sequence generation
    kernel is an aray that contains the values of the kernel for each time of trade. We need its values from 0 to maxlag
    noise is the noise determined by the user for the returns generation
    max_lag is the maximum lag considered by the user/the cutoff. This quantity is crucial because we cut the impact of orderflows below t-maxlag

    '''

    orderflow = pd.Series(orderflow)
    kernel = pd.Series(kernel)
    noise = pd.Series(noise)

    N = len(noise)

    returns = np.zeros(N)
    

    K = kernel.diff(periods = 1).dropna() # G_1-G_0 = K_0, G_2-G_1 = K_1, ...., G_maxlag+1-G_maxlag = K_maxlag

    matrix = orderflows_matrix(orderflow, max_lag)
  

    returns[:max_lag] = matrix.dot(K[:max_lag]) + noise[:max_lag]
    returns[max_lag:] = noise[max_lag:]  + np.convolve(K.iloc[:max_lag+1], orderflow.values , mode = 'valid')


    return pd.Series(returns)


# $\underline{\text{Regression functions:}}$

# we inverse the problem to get the returns kernel
def problem_inversion(S: typing.Union[np.array, pd.Series], C: np.array, L: int) -> pd.Series:

    '''

    S is an array that represents the sign return-correlation.
    C is a matrix, and represents the covariance matrix induced by the autocovariance vector of order flows 
    L is the cutoff considered for the regression using the problem inversion
    
    '''

    K = np.zeros(L+1)
    S = pd.Series(S)

    for l in range(L+1):

        for i in range(L+1):

            K[l] += S.values[i]*C[l,i]
            
    return pd.Series(K)


def regression_matrix(epsilons: typing.Union[np.array, pd.Series], max_lag: int = 1000) -> pd.Series:

    epsilons = pd.Series(epsilons)
    N = len(epsilons)
    matrix = np.zeros((N-max_lag,max_lag+1))

    for lag in range(N-max_lag):

        matrix[lag,:] = epsilons.iloc[lag:max_lag+lag+1].values[::-1]

    return matrix 

# $\underline{Criteria:}$

def R2(mid, approx) -> pd.Series: # R2
    
    sigma = np.mean(np.square(mid))
    
    return( pd.Series(1 - np.mean( np.square( (mid - approx)))/sigma ) )  


